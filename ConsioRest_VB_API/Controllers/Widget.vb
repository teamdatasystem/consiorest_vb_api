﻿Imports System.Data.SqlClient
Imports Dapper
Imports Microsoft.AspNetCore.Http
Imports Microsoft.AspNetCore.Mvc

<Route("[controller]")>
<ApiController>
Public Class Widget

    Inherits ControllerBase

    <ProducesResponseType(StatusCodes.Status200OK)>
    <HttpGet("ConsioWidgets")>
    Public Function GetConsioWidget(Token As String, id As Integer) As IActionResult

        If Startup.GetIPv4Address("ConsioRestAPI", HttpContext.Connection.RemoteIpAddress.ToString) <> "OK" Then
            Return NotFound(Startup.GetIPv4Address("ConsioRestAPI", HttpContext.Connection.RemoteIpAddress.ToString))
        End If

        Dim sSql As String = ""

        Using masterConnection As New SqlConnection(Startup.MasterConn())
            sSql = masterConnection.QuerySingle(Of String)("Select SQL From ConsioWidget Where SeqNo = @SeqNo", New With {Key .SeqNo = id})
        End Using

        Using con As New SqlConnection(Startup.GetCustomerClientConn(Token))
            Return Ok(con.Query(Of ConsioWidgetModel)(sSql))
        End Using

    End Function

End Class