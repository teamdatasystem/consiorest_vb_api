﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Net
Imports System.Text
Imports Dapper
Imports Microsoft.AspNetCore.Http
Imports Microsoft.AspNetCore.Mvc
Imports Newtonsoft.Json

<Route("[controller]")>
<ApiController>
Public Class Consio

    Inherits ControllerBase

    <ProducesResponseType(StatusCodes.Status200OK)>
    <HttpGet("SystemReg")>
    Public Function GetSystemReg(<FromHeader> Token As String, SystemLevel As String, Code As String) As IActionResult

        If Startup.GetIPv4Address("ConsioRestAPI", HttpContext.Connection.RemoteIpAddress.ToString) <> "OK" Then
            Return NotFound(Startup.GetIPv4Address("ConsioRestAPI", HttpContext.Connection.RemoteIpAddress.ToString))
        End If

        Using con As New SqlConnection(Startup.GetCustomerClientConn(Token))
            Return Ok(con.Query(Of SystemRegModel)("SELECT * From SystemReg Where SystemLevel = @SystemLevel And Code = @Code", New With {Key SystemLevel, Code}))
        End Using

    End Function

    <ProducesResponseType(StatusCodes.Status200OK)>
    <HttpGet("Relations")>
    Public Function GetRelations(<FromHeader> Token As String, ActNo As Integer?, Status As String, Category As String) As IActionResult

        If Startup.GetIPv4Address("ConsioRestAPI", HttpContext.Connection.RemoteIpAddress.ToString) <> "OK" Then
            Return NotFound(Startup.GetIPv4Address("ConsioRestAPI", HttpContext.Connection.RemoteIpAddress.ToString))
        End If

        Dim Where As String = "1 = 1"
        If ActNo > 0 Then
            Where += " And ActNo = " & ActNo.ToString
        End If
        If Status > "" Then
            Where += " And OptChar1 In (" & Startup.StringToSQL(Status) & ")"
        End If
        If Category > "" Then
            Where += " And OptChar2 In (" & Startup.StringToSQL(Category) & ")"
        End If

        Using con As New SqlConnection(Startup.GetCustomerClientConn(Token))
            Return Ok(con.Query(Of RelationsModel)("SELECT * From ConsioRestAPI_Relations_v Where " & Where & " Order By Nm", New With {Key Where}))
        End Using

    End Function

    <ProducesResponseType(StatusCodes.Status200OK)>
    <HttpGet("CommitteePosCode")>
    Public Function GetKomiteVerv(<FromHeader> Token As String, ActNo As Integer?, Committy As String, PosCode As String) As IActionResult

        If Startup.GetIPv4Address("ConsioRestAPI", HttpContext.Connection.RemoteIpAddress.ToString) <> "OK" Then
            Return NotFound(Startup.GetIPv4Address("ConsioRestAPI", HttpContext.Connection.RemoteIpAddress.ToString))
        End If

        Dim Where As String = "1 = 1"
        If ActNo > 0 Then
            Where += " And ActNo = " & ActNo.ToString
        End If
        If Committy > "" Then
            Where += " And Commity In (" & Startup.StringToSQL(Committy) & ")"
        End If
        If PosCode > "" Then
            Where += " And PosCode In (" & Startup.StringToSQL(PosCode) & ")"
        End If

        Using con As New SqlConnection(Startup.GetCustomerClientConn(Token))
            Return Ok(con.Query(Of KomiteVervModel)("SELECT * From ConsioRestAPI_ActPos_v Where " & Where & " Order By ActNo, Sorting", New With {Key Where}))
        End Using

    End Function

    <ProducesResponseType(StatusCodes.Status200OK)>
    <HttpGet("RelationsActors")>
    Public Function GetRelationActors(<FromHeader> Token As String, ActNo As Integer?, ActorType As String, RelationType As String) As IActionResult

        If Startup.GetIPv4Address("ConsioRestAPI", HttpContext.Connection.RemoteIpAddress.ToString) <> "OK" Then
            Return NotFound(Startup.GetIPv4Address("ConsioRestAPI", HttpContext.Connection.RemoteIpAddress.ToString))
        End If

        Dim Where As String = "1 = 1"
        If ActNo > 0 Then
            Where += " And ActNo = " & ActNo.ToString
        End If
        If ActorType > "" Then
            Where += " And ActorType In (" & Startup.StringToSQL(ActorType) & ")"
        End If
        If RelationType > "" Then
            Where += " And RelType In (" & Startup.StringToSQL(RelationType) & ")"
        End If

        Using con As New SqlConnection(Startup.GetCustomerClientConn(Token))
            Return Ok(con.Query(Of RelationActorsModel)("SELECT * From ConsioRestAPI_RelationActors_v Where " & Where & " Order By Nm", New With {Key Where}))
        End Using

    End Function

    <ProducesResponseType(StatusCodes.Status200OK)>
    <HttpGet("ActorRoles")>
    Public Function GetRoles(<FromHeader> Token As String, <FromHeader> Bearer As String) As IActionResult

        Dim WebRequest As HttpWebRequest
        Dim myHttpWebRequest As String, IdServer As String = ""

        Using con As New SqlConnection(Startup.GetCustomerClientConn(Token))
        End Using

        myHttpWebRequest = IdServer
        Dim targetURI As New Uri(myHttpWebRequest)
        Bearer = "Bearer " & Bearer
        WebRequest = DirectCast(HttpWebRequest.Create(targetURI), HttpWebRequest)
        WebRequest.Headers.Add("Authorization", Bearer)
        WebRequest.ContentType = "application/json"
        WebRequest.Method = "GET"

        Dim response = WebRequest.GetResponse()
        Dim origResponse = DirectCast(WebRequest.GetResponse(), HttpWebResponse)
        Dim Stream As Stream = origResponse.GetResponseStream()
        Dim sr As New StreamReader(Stream, Encoding.GetEncoding("utf-8"))
        Dim str As String = sr.ReadToEnd()
        Dim jsonResult = JsonConvert.DeserializeObject(Of Dictionary(Of String, Object))(str)
        Dim UserNm = CType(jsonResult.Item("UserName"), String)
        Dim ORgSeqNo = 1
        If UserNm.Contains("@") Then
            UserNm.Substring(0, UserNm.IndexOf($"@{ORgSeqNo}"))
        End If

        Return NotFound("Ikke implementert")

    End Function

End Class