﻿Imports System.Data.SqlClient
Imports System.Security.Cryptography
Imports Dapper
Imports Microsoft.AspNetCore.Http
Imports Microsoft.AspNetCore.Mvc
Imports Microsoft.AspNetCore.Mvc.Infrastructure

<Route("[controller]")>
<ApiController>
Public Class ConsioSports

    Inherits ControllerBase

    <ProducesResponseType(StatusCodes.Status200OK)>
    <HttpGet("Members")>
    Public Function GetSportMembers(<FromHeader> Token As String, ActNo As Integer, StatusCode As String, MobPh As String, Email As String) As IActionResult

        If Startup.GetIPv4Address("ConsioSports", HttpContext.Connection.RemoteIpAddress.ToString) <> "OK" Then
            Return NotFound(Startup.GetIPv4Address("ConsioSports", HttpContext.Connection.RemoteIpAddress.ToString))
        End If

        Dim Where As String = "1 = 1"
        If ActNo > 0 Then
            Where += " And ActNo = " & ActNo.ToString
        End If
        If StatusCode > "" Then
            Where += " And OptChar1 In (" & Startup.StringToSQL(StatusCode) & ")"
        End If
        If MobPh > "" Then
            Where += " And MobPh In (" & Startup.StringToSQL(MobPh) & ")"
        End If
        If Email > "" Then
            Where += " And Email In (" & Startup.StringToSQL(Email) & ")"
        End If

        Try
            Using con As New SqlConnection(Startup.GetCustomerClientConn(Token))
                Return Ok(con.Query(Of MemberModel)("SELECT * From ConsioRestAPI_SportMembers_v Where " & Where & " Order By Nm, FiNm"))
            End Using
        Catch ex As Exception
            Return NotFound(ex.Message)
        End Try

    End Function

    <ProducesResponseType(StatusCodes.Status200OK)>
    <HttpGet("MemberRoles")>
    Public Function GetSportMemberRoles(<FromHeader> Token As String, ActNo As Integer, StatusCode As String) As IActionResult

        If Startup.GetIPv4Address("ConsioSports", HttpContext.Connection.RemoteIpAddress.ToString) <> "OK" Then
            Return NotFound(Startup.GetIPv4Address("ConsioSports", HttpContext.Connection.RemoteIpAddress.ToString))
        End If

        Dim Where As String = "1 = 1"
        If ActNo > 0 Then
            Where += " And ActNo = " & ActNo.ToString
        End If
        If StatusCode > "" Then
            Where += " And OptChar1 In (" & Startup.StringToSQL(StatusCode) & ")"
        End If

        Try
            Using con As New SqlConnection(Startup.GetCustomerClientConn(Token))
                Return Ok(con.Query(Of MemberRolesModel)("SELECT * From ConsioRestAPI_SportMemberRoles_v Where " & Where & " Order By ActNo, RelSortOrder"))
            End Using
        Catch ex As Exception
            Return NotFound(ex.Message)
        End Try

    End Function


    <ProducesResponseType(StatusCodes.Status200OK)>
    <HttpGet("MemberUpdate")>
    Public Function UpdateMember(<FromHeader> Token As String, ActNo As Integer, MobPh As String) As IActionResult

        If Startup.GetIPv4Address("ConsioSports", HttpContext.Connection.RemoteIpAddress.ToString) <> "OK" Then
            Return NotFound(Startup.GetIPv4Address("ConsioSports", HttpContext.Connection.RemoteIpAddress.ToString))
        End If

        Try
            Using con As New SqlConnection(Startup.GetCustomerClientConn(Token))
                con.Query("Update ActNo Set MobPh = @MobPh Where ActNo = @ActNo", New With {ActNo, MobPh})
                Return Ok(con.Query(Of MemberUpdateMobPh)("MobPh Oppdatert OK for nummer " & ActNo.ToString))
            End Using
        Catch ex As Exception
            Return NotFound(ex.Message)
        End Try

    End Function

End Class