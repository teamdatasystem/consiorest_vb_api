﻿Imports System.Data.SqlClient
Imports Dapper
Imports Microsoft.AspNetCore.Http
Imports Microsoft.AspNetCore.Mvc

<Route("api/[controller]")>
<ApiController>
Public Class VNRController

    Inherits ControllerBase

    <ProducesResponseType(StatusCodes.Status200OK)>
    <HttpGet>
    Public Function GetVNRMembers(<FromHeader> Token As String) As IActionResult

        Using con As New SqlConnection(Startup.GetCustomerClientConn(Token))
            Return Ok(con.Query(Of VNRMember)("SELECT ActNo, Nm, Email, Web From VNR_API_v"))
        End Using

    End Function

End Class