﻿Imports System.Data.SqlClient
Imports Dapper
Imports Microsoft.AspNetCore.Builder
Imports Microsoft.AspNetCore.Hosting
Imports Microsoft.AspNetCore.Mvc
Imports Microsoft.Extensions.Configuration
Imports Microsoft.Extensions.DependencyInjection
Imports Microsoft.Extensions.Hosting
Imports Microsoft.OpenApi.Models

Public Class Startup
    Public Sub New(ByVal configuration As IConfiguration)
#Disable Warning BC42025 ' Access of shared member, constant member, enum member or nested type through an instance
        Me.Configuration = configuration
#Enable Warning BC42025 ' Access of shared member, constant member, enum member or nested type through an instance
    End Sub

    Public Shared Property Configuration As IConfiguration

    Public Sub ConfigureServices(ByVal services As IServiceCollection)

        services.AddSwaggerGen(Sub(prm) prm.SwaggerDoc("v1", New OpenApiInfo With {.Title = "ConsioRestAPI", .Version = "v1"}))
        services.AddMvc(Sub(opt) opt.EnableEndpointRouting = False).SetCompatibilityVersion(CompatibilityVersion.Latest)

    End Sub

    Public Sub Configure(ByVal app As IApplicationBuilder, ByVal env As IHostEnvironment)
        If env.IsDevelopment() Then
            app.UseDeveloperExceptionPage()
        End If
        app.UseSwagger()
        app.UseSwaggerUI(Sub(prm) prm.SwaggerEndpoint("../swagger/v1/swagger.json", "ConsioRest_VB_API V1"))
        app.UseAuthentication()
        app.UseMvc()
    End Sub

    Public Shared Function MasterConn()
        Return Configuration.GetConnectionString("MasterConnection")
    End Function

    Public Shared Function GetCustomerClientConn(ByVal NetPayToken As String) As String
        Dim connString = MasterConn()

        Using masterConnection = New SqlConnection(MasterConn)
            connString = masterConnection.QuerySingle(Of CustomerConnectionstring)("SELECT SQLServer, SQLDB, SQLUser, SQLpwd FROM Organization WHERE NetPayToken=@NetPayToken", New With {Key NetPayToken}).ToString()
        End Using

        Return connString
    End Function
    Public Shared Function StringToSQL(ByVal Kode As String) As String

        Dim words As String() = Kode.Split(New Char() {","c}), ReturnCode As String = ""

        Dim word As String
        For Each word In words
            If ReturnCode > "" Then
                ReturnCode += ","
            End If
            ReturnCode += "'" & word & "'"
        Next

        Return ReturnCode

    End Function

    Public Shared Function GetIPv4Address(ConsioSystem As String, IP As String, Optional Token As String = "") As String

        Using masterConnection = New SqlConnection(MasterConn)
            Try
                IP = masterConnection.QuerySingle(Of String)("Select IPAddress FROM IPAddressAPIWhitelist WHERE IpAddress = @IP And OrgNummer = @ConsioSystem", New With {Key IP, ConsioSystem})
                Return "OK"
            Catch ex As Exception
                masterConnection.Query("INSERT INTO IPAddressAPIErrorLog (ConsioSystem,[IP],Token) VALUES (@ConsioSystem,@IP,@Token)", New With {ConsioSystem, IP, Token})
                Return "Feil IP " & IP & " - " & ConsioSystem & " token " & Token
            End Try
        End Using

    End Function

    Public Class CustomerConnectionstring
        Public Property SQLServer As String
        Public Property SQLDB As String
        Public Property SQLUser As String
        Public Property SQLpwd As String

        Public Overrides Function ToString() As String
            Return $"Data Source={SQLServer};Initial Catalog={SQLDB};User ID={SQLUser};Password={SQLpwd};"
        End Function

    End Class

End Class

