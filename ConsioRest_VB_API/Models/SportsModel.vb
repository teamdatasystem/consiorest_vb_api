﻿Public Class MemberModel
	Public Property ActNo As Integer
	Public Property StatusCode As String
	Public Property StatusNm As String
	Public Property Nm As String
	Public Property FiNm As String
	Public Property Bdt As Date?
	Public Property Sex As String
	Public Property A1 As String
	Public Property A2 As String
	Public Property A3 As String
	Public Property PCode As String
	Public Property PArea As String
	Public Property MobPh As String
	Public Property Email As String
	Public Property StartDt As Date?
	Public Property EndDt As Date?

End Class

Public Class MemberRolesModel
	Public Property ActNo As Integer
	Public Property OptChar1 As String
	Public Property RelType As String
	Public Property RelTypeNm As String
	Public Property RelActNo As Integer
	Public Property Nm As String
	Public Property RelFrDt As Date?
	Public Property RelToDt As Date?
	Public Property RelSortOrder As Integer?

End Class
Public Class MemberUpdateMobPh

	Public Property Status As String

End Class