﻿Public Class SystemRegModel
	Public Property SeqNo As Integer
	Public Property SystemLevel As String
	Public Property Code As String
	Public Property SystemInt As Integer
	Public Property SystemChar As String
	Public Property Nm As String
	Public Property OptChar1 As String
	Public Property OptChar2 As String
	Public Property OptChar3 As String
	Public Property OptInt1 As Integer
	Public Property OptInt2 As Integer
	Public Property OptInt3 As Integer
	Public Property OptDate1 As DateTime
	Public Property OptDate2 As DateTime

End Class
Public Class KomiteVervModel
	Public Property ActNo As Integer
	Public Property Nm As String
	Public Property ActNoPerson As Integer
	Public Property FiNmPerson As String
	Public Property NmPerson As String
	Public Property MobPh As String
	Public Property Email As String
	Public Property Commity As String
	Public Property CommityNm As String
	Public Property PosCode As String
	Public Property PosNm As String
	Public Property StartDt As Date?
	Public Property EndDt As Date?
	Public Property Contact As Boolean?
	Public Property Sortering As Integer?

End Class
Public Class RelationsModel
	Public Property ActNo As Integer
	Public Property Nm As String
	Public Property ContactName As String
	Public Property A1 As String
	Public Property A2 As String
	Public Property A3 As String
	Public Property Pcode As String
	Public Property Parea As String
	Public Property County1No As String
	Public Property County1Name As String
	Public Property County2No As String
	Public Property County2Name As String
	Public Property Ph As String
	Public Property MobPh As String
	Public Property OrgNo As String
	Public Property AccNo As String
	Public Property Email As String
	Public Property Web As String
	Public Property StatusCode As String
	Public Property KategoriCode As String
	Public Property OptDate1 As Date?
	Public Property OptDate2 As Date?
	Public Property Facebook As String
	Public Property Instagram As String
	Public Property Twitter As String

End Class
Public Class RelationActorsModel
	Public Property ActNo As Integer
	Public Property Nm As String
	Public Property FiNm As String
	Public Property RelType As String
	Public Property RelActNo As Integer
	Public Property RelActNm As String
	Public Property RelActFiNm As String
	Public Property RelContactName As String
	Public Property RelA1 As String
	Public Property RelA2 As String
	Public Property RelA3 As String
	Public Property RelPcode As String
	Public Property RelParea As String
	Public Property RelCounty1No As String
	Public Property RelCounty1Name As String
	Public Property RelCounty2No As String
	Public Property RelCounty2Name As String
	Public Property RelPh As String
	Public Property RelMobPh As String
	Public Property RelOrgNo As String
	Public Property RelAccNo As String
	Public Property RelEmail As String
	Public Property RelWeb As String
	Public Property RelStatusCode As String
	Public Property RelKategoriCode As String
	Public Property RelOptDate1 As Date?
	Public Property RelOptDate2 As Date?
	Public Property RelFacebook As String
	Public Property RelInstagram As String
	Public Property RelTwitter As String
	Public Property RelActorType As String
	Public Property RelFrDt As Date?
	Public Property RelToDt As Date?
End Class

Public Class ConsioWidgetModel
	Public Property data As String
	Public Property label As String
	Public Property Innmeldinger As Integer?
	Public Property Utmeldinger As Integer?
	Public Property Inn As Integer?
	Public Property Ut As Integer?
	Public Property Kontingent As Integer?
	Public Property Forsikring As Integer?

End Class