Imports Microsoft.AspNetCore.Hosting
Imports Microsoft.Extensions.Hosting

Module Program
    Public Sub Main(args As String())
        CreateHostBuilder(args).Build().Run()
    End Sub

    Public Function CreateHostBuilder(ByVal args As String()) As IHostBuilder
        Return Host.
            CreateDefaultBuilder(args).
            ConfigureWebHostDefaults(Function(webBuilder)
                                         Return webBuilder.UseStartup(Of Startup)()
                                     End Function)
    End Function

End Module
